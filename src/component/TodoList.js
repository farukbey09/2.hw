import React,{useState} from 'react'
import TodoForm from './TodoForm'
import TodoItem from './TodoItem';

function TodoList() {
    const[todos,setTodos]=useState([]);
    const removeTodo=id=>{
        const removeArr =[...todos].filter(todo=>todo.id!==id);
        setTodos(removeArr);
    };
    const completeTodo=id=>{
        let updatedTodos=todos.map(todo=>{
            if(todo.id===id){
                todo.isComplete=!todo.isComplete;
            }
            return todo;
        })
        setTodos(updatedTodos);
    }
    const addTodo =todo=>{
        if(!todo.text|| /^\s*$/.test(todo.text)){
            return;
        }
        const newTodos=[todo, ...todos];
        setTodos(newTodos);

       
     };

    return (
        <div>
            <h1 className="header" >My Todo List</h1>
        <TodoForm onSubmit={addTodo}/> 
        <TodoItem todos={todos} 
        completeTodo={completeTodo} 
        removeTodo={removeTodo}
        />
        
        </div>
    )
}

export default TodoList
